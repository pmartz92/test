import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: "home",
    templateUrl: "home.component.html"
})

export class Home  {
    constructor(private activatedRoute: ActivatedRoute, private router: Router) { }
}