import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: "profile",
    templateUrl: "profile.component.html"
})

export class Profile  {
    constructor(private activatedRoute: ActivatedRoute, private router: Router) { }
}