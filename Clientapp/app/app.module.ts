import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule, MatButtonModule } from '@angular/material';
import { RouterModule } from "@angular/router";
import { Profile } from "./profile/profile.component";
import { Home } from "./home/home.component";

import { AppComponent } from './app.component';

let routes = [
  { path: "", component: Home },
  { path: "profile", component: Profile }
];


@NgModule({
  declarations: [
    AppComponent,
    Profile,
    Home
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    RouterModule.forRoot(routes, {
      useHash: true,
      enableTracing: false // for Debugging of the Routes
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
